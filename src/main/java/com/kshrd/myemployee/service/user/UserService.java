package com.kshrd.myemployee.service.user;

import com.kshrd.myemployee.model.Pagination;
import com.kshrd.myemployee.model.Users;
import com.kshrd.myemployee.payload.BaseApiResponse;
import com.kshrd.myemployee.payload.user.UserApiRequest;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    Users findByUsername(String username);

    BaseApiResponse<Users> insertUser(Users user);

    BaseApiResponse<List<Users>> findAllUsers(Pagination pagination);

    BaseApiResponse<Users> findUserById(Integer id);

    BaseApiResponse<Users> updateUser(Integer id, UserApiRequest user);

    BaseApiResponse<Users> deleteUser(Integer id);

    Integer countAllUsers();

}
