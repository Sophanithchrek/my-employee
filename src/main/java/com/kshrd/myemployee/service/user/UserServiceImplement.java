package com.kshrd.myemployee.service.user;

import com.kshrd.myemployee.model.Pagination;
import com.kshrd.myemployee.model.Users;
import com.kshrd.myemployee.payload.BaseApiResponse;
import com.kshrd.myemployee.payload.user.UserApiRequest;
import com.kshrd.myemployee.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImplement implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Users user = userRepository.findByUsername(username);
        if(user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return user;

    }

    @Override
    public Users findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Integer countAllUsers() {

        return userRepository.countAllUsers();

    }

    @Override
    public BaseApiResponse<Users> insertUser(Users user) {

        BaseApiResponse response = new BaseApiResponse();

        try {

            Boolean value = userRepository.insertUser(user);

            if (value) {
                response.setMessage("User has been inserted successfully.");
                response.setData(user);
                response.setStatus(true);
                response.setHttpStatus(HttpStatus.CREATED);
            } else {
                response.setMessage("User is not insert.");
                response.setStatus(false);
                response.setHttpStatus(HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            response.setMessage("Error : " + e.getMessage());
            response.setStatus(false);
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
        }

        return response;

    }

    @Override
    public BaseApiResponse<List<Users>> findAllUsers(Pagination pagination) {

        BaseApiResponse response = new BaseApiResponse();

        try {

            List<Users> value = userRepository.findAllUsers(pagination);

            if (!value.isEmpty()) {
                response.setMessage("Users has been found successfully.");
                response.setData(value);
                response.setStatus(true);
                response.setHttpStatus(HttpStatus.OK);
            } else {
                response.setMessage("Users data is empty.");
                response.setData(value);
                response.setStatus(false);
                response.setHttpStatus(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            response.setMessage("Error : " + e.getMessage());
            response.setStatus(false);
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
        }

        response.setPagination(pagination);
        return response;

    }

    @Override
    public BaseApiResponse<Users> findUserById(Integer id) {

        BaseApiResponse response = new BaseApiResponse();

        try {

            Users value = userRepository.findUserById(id);

            if (value != null) {
                response.setMessage("User has been found successfully.");
                response.setData(value);
                response.setStatus(true);
                response.setHttpStatus(HttpStatus.OK);
            } else {
                response.setMessage("User data is empty.");
                response.setData(value);
                response.setStatus(false);
                response.setHttpStatus(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            response.setMessage("Error : " + e.getMessage());
            response.setStatus(false);
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
        }

        return response;

    }

    @Override
    public BaseApiResponse<Users> updateUser(Integer id, UserApiRequest user) {

        BaseApiResponse response = new BaseApiResponse();

        try {

            Boolean value = userRepository.updateUser(id, user);

            if (value) {
                response.setMessage("User has been updated successfully.");
                response.setData(userRepository.findUserById(id));
                response.setStatus(true);
                response.setHttpStatus(HttpStatus.OK);
            } else {
                response.setMessage("User is not update.");
                response.setStatus(false);
                response.setHttpStatus(HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            response.setMessage("Error : " + e.getMessage());
            response.setStatus(false);
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
        }

        return response;

    }

    @Override
    public BaseApiResponse<Users> deleteUser(Integer id) {

        BaseApiResponse response = new BaseApiResponse();

        try {

            Boolean value = userRepository.deleteUser(id);

            if (value) {
                response.setMessage("User has been deleted successfully.");
                response.setData(userRepository.findUserById(id));
                response.setStatus(true);
                response.setHttpStatus(HttpStatus.OK);
            } else {
                response.setMessage("User is not delete.");
                response.setStatus(false);
                response.setHttpStatus(HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            response.setMessage("Error : " + e.getMessage());
            response.setStatus(false);
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
        }

        return response;

    }

}
