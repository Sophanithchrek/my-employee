package com.kshrd.myemployee.repository;

import com.kshrd.myemployee.model.Pagination;
import com.kshrd.myemployee.model.Roles;
import com.kshrd.myemployee.model.Users;
import com.kshrd.myemployee.payload.user.UserApiRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {

     @SelectProvider(type = UserProvider.class, method = "findByUsername")
     @Results({
             @Result(property = "id", column = "id"),
             @Result(property = "roles", column = "role_id", many = @Many(select = "findRoleByUserId"))
     })
     Users findByUsername(@Param("username") String username);

     @Select("SELECT id, role_name FROM Roles WHERE id = #{id}")
     @Results({
             @Result(property = "roleName", column = "role_name")
     })
     List<Roles> findRoleByUserId(int id);

     // User's status: 1 => 'Active', 9 => 'Deleted'

     @InsertProvider(type = UserProvider.class, method = "insertUser")
     @Results(id = "requestMap", value = {
             @Result(property = "createdOn", column = "created_on"),
             @Result(property = "lastLogin", column = "last_login")
     })
     Boolean insertUser(@Param("user") Users user);

     @SelectProvider(type = UserProvider.class, method = "findAllUsers")
     @Results(id = "resultMap", value = {
             @Result(property = "createdOn", column = "created_on"),
             @Result(property = "lastLogin", column = "last_login")
     })
     List<Users> findAllUsers(@Param("pagination") Pagination pagination);

     @Select("SELECT * FROM users WHERE id = #{userId} AND status = '1'")
     @ResultMap("resultMap")
     Users findUserById(@Param("userId") Integer id);

     @UpdateProvider(type = UserProvider.class, method = "updateUser")
     Boolean updateUser(@Param("userId") Integer id, @Param("user") UserApiRequest user);

     @Delete("UPDATE users SET status = '9' WHERE id = #{userId}")
     Boolean deleteUser(@Param("userId") Integer id);

     @Select("SELECT COUNT(*) FROM users WHERE status = '1'")
     Integer countAllUsers();

}
