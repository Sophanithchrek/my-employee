package com.kshrd.myemployee.repository;

import org.apache.ibatis.jdbc.SQL;

public class UserProvider {

    public String findByUsername() {
        return new SQL(){{
            SELECT ("u.id, u.username, u.email, u.password, u.status, u.role_id, r.role_name");
            FROM ("Users u INNER JOIN Roles r ON u.role_id = r.id");
            WHERE ("u.username = #{username}");
        }}.toString();
    }

    public String insertUser() {
        return new SQL(){{
            INSERT_INTO("users");
            VALUES("username, password, email, created_on, last_login",
                    "#{user.username}, #{user.password}, #{user.email}, #{user.createdOn}, #{user.lastLogin}");
        }}.toString();
    }

    public String findAllUsers() {
        return new SQL(){{
            SELECT("*");
            FROM("users");
            WHERE("status = '1'");
        }}.toString();
    }

    public String updateUser() {
        return new SQL(){{
            UPDATE("users");
            SET("username = #{user.username}, " +
                    "password = #{user.password}," +
                    "email = #{user.email}");
            WHERE("id = #{userId}");
        }}.toString();
    }

}
