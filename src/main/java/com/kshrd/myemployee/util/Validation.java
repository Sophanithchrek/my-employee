package com.kshrd.myemployee.util;

import org.springframework.stereotype.Component;

@Component
public class Validation {

    public boolean checkInputString(String input) {
        return (input == null || input.trim().length() == 0);
    }

    public boolean checkInputPassword(String input) {
        return (input==null || input.trim().length() == 0 || input.length() < 4);
    }
}
