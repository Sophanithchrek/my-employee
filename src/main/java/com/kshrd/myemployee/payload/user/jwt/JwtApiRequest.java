package com.kshrd.myemployee.payload.user.jwt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JwtApiRequest {

    private String username;

    private String password;

}
