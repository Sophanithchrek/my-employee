package com.kshrd.myemployee.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Roles implements GrantedAuthority {

    private Integer id;

    private String roleName;

    private String status;

    @Override
    public String getAuthority() {
        return "ROLE_" + roleName;
    }

}