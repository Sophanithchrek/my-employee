package com.kshrd.myemployee;

import com.kshrd.myemployee.property.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class MyemployeeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyemployeeApplication.class, args);

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String pass = encoder.encode("admin");
        System.out.println(">>>> Password encoded: "+pass);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
