package com.kshrd.myemployee.controller;

import com.kshrd.myemployee.model.FileInfo;
import com.kshrd.myemployee.payload.BaseApiResponse;
import com.kshrd.myemployee.service.file.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1")
public class FileController {

    @Autowired
    private FileStorageService fileStorageService;

    @Value(value = "${file.base.url}")
    private String BaseUrl;

    @PostMapping("/upload")
    public BaseApiResponse<FileInfo> uploadFile(@RequestPart("file") MultipartFile file) {

        BaseApiResponse response = new BaseApiResponse();

        System.out.println("Image : " + file.getOriginalFilename());
        String fileName;
        FileInfo fileInfo = new FileInfo();

        try {
            fileName = fileStorageService.save(file);
            response.setMessage("File has been uploaded successfully.");
            response.setStatus(true);
            response.setHttpStatus(HttpStatus.OK);

            fileInfo.setImageId(fileName);
            fileInfo.setImageUrl(BaseUrl + fileName);

            response.setData(fileInfo);

        } catch (Exception e) {
            response.setMessage("Could not upload the file: " + file.getOriginalFilename() + "!");
            response.setStatus(false);
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
        }

        return response;
    }

//    @PostMapping("/upload-multiple-files")
//    public List<FileInfo> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
//
//        return (List<FileInfo>) Arrays.asList(files)
//                .stream()
//                .map(file -> uploadFile(file))
//                .collect(Collectors.toList());
//
//    }

//    @PostMapping("/upload-multiple-files")
//    public ResponseEntity<Map<String,Object>> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
//
//        Map<String, Object> res = new HashMap<>();
//
//        int i=0;
//
//        try {
//            for(MultipartFile file : files)
//            {
//                i++;
//                String fileName = fileStorageService.save(file);
//
//                if(i==1){
//                    res.put("message","File have been saved successfully");
//                    res.put("status","Ok");
//                }
//                res.put("file"+i,(BaseUrl+fileName));
//            }
//
//            res.put("time",new Timestamp(System.currentTimeMillis()));
//
//            return ResponseEntity.status(HttpStatus.OK).body(res);
//
//        } catch (Exception e) {
//
//            res.put("message","Could not upload the file:");
//            res.put("status",false);
//            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(res);
//        }
//    }

    @PostMapping("/upload-multiple-files")
    public BaseApiResponse<List<FileInfo>> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {

        BaseApiResponse response = new BaseApiResponse();

        try {

            response.setMessage("File have been saved successfully");
            response.setStatus(true);
            response.setHttpStatus(HttpStatus.OK);

            List<FileInfo> fileInfoList = new ArrayList<>();

            for(MultipartFile file : files) {

                String fileName = fileStorageService.save(file);
                FileInfo fileInfo = new FileInfo();
                fileInfo.setImageId(fileName);
                fileInfo.setImageUrl(BaseUrl + fileName);
                fileInfoList.add(fileInfo);

            }

            response.setData(fileInfoList);
            return response;

        } catch (Exception e) {
            response.setMessage("Could not upload the file!");
            response.setStatus(false);
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
            return null;
        }

    }


    // ----

//    @GetMapping("/files")
//    public ResponseEntity<List<FileInfo>> getListFiles() {
//
//        List<FileInfo> myFileUploadResponses = fileStorageService.loadAll().map(path -> {
//
//            String filename = path.getFileName().toString();
//
//            return new FileInfo("File has been found successfully", true, filename, BaseUrl + filename);
//
//        }).collect(Collectors.toList());
//
//        return ResponseEntity.status(HttpStatus.OK).body(myFileUploadResponses);
//    }

    @GetMapping("/files")
    public ResponseEntity<BaseApiResponse<List<FileInfo>>> getListFiles() {

        BaseApiResponse response = new BaseApiResponse();

        try {

            response.setMessage("All file has been found successfully");
            response.setStatus(true);
            response.setHttpStatus(HttpStatus.FOUND);

            List<FileInfo> myFileUploadResponses = fileStorageService.loadAll().map(path -> {
                String filename = path.getFileName().toString();
                return new FileInfo(filename, BaseUrl + filename);
            }).collect(Collectors.toList());
            response.setData(myFileUploadResponses);

            return ResponseEntity.ok(response);

        } catch (Exception e){
            response.setMessage("No image");
            response.setData("");
            response.setHttpStatus(HttpStatus.NOT_FOUND);
            return null;
        }

    }

}