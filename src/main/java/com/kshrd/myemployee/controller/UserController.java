package com.kshrd.myemployee.controller;

import com.kshrd.myemployee.component.jwt.JwtUtility;
import com.kshrd.myemployee.model.Pagination;
import com.kshrd.myemployee.model.Users;
import com.kshrd.myemployee.payload.BaseApiResponse;
import com.kshrd.myemployee.payload.user.UserApiRequest;
import com.kshrd.myemployee.payload.user.UserLoginApiRequest;
import com.kshrd.myemployee.payload.user.jwt.JwtApiResponse;
import com.kshrd.myemployee.service.user.UserServiceImplement;
import com.kshrd.myemployee.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class UserController {

    @Autowired
    private UserServiceImplement userService;

    @Autowired
    private Validation validation;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtility jwtUtility;

    @PostMapping("/users/authenticate")
    public ResponseEntity<BaseApiResponse<HashMap<String,Object>>> createAuthenticationToken(@RequestBody UserLoginApiRequest user) {

        BaseApiResponse<HashMap<String,Object>> response = new BaseApiResponse<>();

        try {

            boolean username = validation.checkInputString(user.getUsername());
            boolean password = validation.checkInputPassword(user.getPassword());

            if (username || password) {

                if (username) {
                    if (password)
                        response.setMessage("Username and password are invalid.");
                    else
                        response.setMessage("Username is invalid.");
                } else {
                    response.setMessage("Password should have at least 8 characters.");
                }

                response.setStatus(false);
                response.setHttpStatus(HttpStatus.BAD_REQUEST);
                return ResponseEntity.ok(response);

            } else {

                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(user.getUsername(),user.getPassword());
                authenticationManager.authenticate(usernamePasswordAuthenticationToken);

                final UserDetails userDetails = userService.loadUserByUsername(user.getUsername());
                Users users = userService.findByUsername(user.getUsername());
                final String token = jwtUtility.generateToken(userDetails);

                JwtApiResponse value = new JwtApiResponse(token);
                value.setJwtToken("Bearer "+ token);

                HashMap<String,Object> data = new HashMap<>();
                data.put("id",users.getId());
                data.put("username",userDetails.getUsername());
                data.put("roles",userDetails.getAuthorities());
                data.put("data",value);

                response.setMessage("Login is successfully.");
                response.setData(data);
                response.setStatus(true);
                response.setHttpStatus(HttpStatus.OK);

            }

        } catch (BadCredentialsException e) {
            response.setMessage("Invalid username or password.");
            response.setStatus(false);
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(response);

    }

    @PostMapping("users")
    public ResponseEntity<BaseApiResponse<Users>> insertUser(@RequestBody Users user) {
        return ResponseEntity.ok(userService.insertUser(user));
    }

    @GetMapping("users")
    public ResponseEntity<BaseApiResponse<List<Users>>> findAllUsers(
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "limit", required = false, defaultValue = "5") int limit) {

        Pagination pagination = new Pagination(page, limit);

        pagination.setPage(page);
        pagination.setLimit(limit);

        pagination.setTotalCount(userService.countAllUsers());
        pagination.setTotalPages(pagination.getTotalPages());

        return ResponseEntity.ok(userService.findAllUsers(pagination));
    }

    @GetMapping("users/{id}")
    public ResponseEntity<BaseApiResponse<Users>> findUserById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(userService.findUserById(id));
    }

    @PutMapping("users/{id}")
    public ResponseEntity<BaseApiResponse<Users>> updateUser(@PathVariable("id") Integer id, @RequestBody UserApiRequest user) {
        return ResponseEntity.ok(userService.updateUser(id, user));
    }

    @DeleteMapping("users/{id}")
    public ResponseEntity<BaseApiResponse<Users>> deleteUser(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(userService.deleteUser(id));
    }

}
